# build-info
Build information about packages for Sun/OS Linux

## Kernel
[![Copr build status](https://copr.fedorainfracloud.org/coprs/abdonmorales/kernel/package/kernel/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/abdonmorales/kernel/package/kernel/)

## GCC
[![Copr build status](https://copr.fedorainfracloud.org/coprs/abdonmorales/gcc/package/gcc/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/abdonmorales/gcc/package/gcc/)

## Anaconda
[![Copr build status](https://copr.fedorainfracloud.org/coprs/abdonmorales/anaconda/package/anaconda/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/abdonmorales/anaconda/package/anaconda/)

## Libreport
[![Copr build status](https://copr.fedorainfracloud.org/coprs/abdonmorales/libreport/package/libreport/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/abdonmorales/libreport/package/libreport/)

## Abort
[![Copr build status](https://copr.fedorainfracloud.org/coprs/abdonmorales/abrt/package/abrt/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/abdonmorales/abrt/package/abrt/)
